package pl.codementors.comics;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * Represents comics library. Contains inner collection with comics, exposes methods for adding and removing comics.
 * Allows for changing covers of all contained comics.
 *
 * @author psysiu
 */
public class ComicsLibrary {

    /*** Set of comics contained in the library.*/
    private Set<Comic> comics = new HashSet<>();

    /**** @return All comics in the library. The returned collection
     * is unmodifiable so it can not be changed* outside the library.*/
    public Collection<Comic> getComics() {
        return comics;
    }

    /*** Adds comic to the library. If comic is already in the library does nothing.
     *  If comic is null does nothing.
     ** @param comic Comic to be added to the library.
     */
    public void add(Comic comic) {
        if (comic != null) {
            comics.add(comic);
        }


    }

    /*** Removes comic from the library. If comics is not present in the library does nothing.*
     * @param comic Comic to be removed from the library.*/

    public void remove(Comic comic) {
        comics.remove(comic);
    }

    /*** Changes covers of all comics in the library.*
     * @param cover Cover type for all comics in the library.*/

    public void changeCovers(Comic.Cover cover) {

        for (Comic comic : comics) {
            comic.setCover(cover);
        }
    }

    /*** @return All authors of all comics in the library. Each author is present only once in the returned collection.
     * The returned collection is unmodifiable so it can not be changed outside the library.*/

    public Collection<String> getAuthors() {
        Collection<String> authors = new HashSet<>();

        for (Comic comic : comics) {
            authors.add(comic.getAuthor());
        }
        return authors;
    }

    /**** @return All series of all comics in the library. Each series is present only once in the returned collection.
     * The returned collection is unmodifiable so it can not be changed outside the library.*/
    public Collection<String> getSeries() {
        //throw new UnsupportedOperationException();
        Collection<String> series = new HashSet<>();
        for (Comic comic : comics) {
            series.add(comic.getSeries());
        }
        return series;
    }

    /**
     * Loads comics from file. Method uses FileReader and Scanner for reading comics from file.
     * <p>
     * The file structure is:
     * number_of_comics (one line with one number, nextInt())
     * comics title (one line with spaces, nextLine())
     * comics author (one line with spaces, nextLine())
     * comics series (one line with spaces, nextLine())
     * cover (one line with one word, next())
     * publish_month (one line with one number, nextInt())
     * publish_year (one line with one number, nextInt())
     * <p>
     * The proper sequence for reading file is to call nextInt(); skip("\n"); to read number of comics.
     * Then in loop call nextLine(); nextLine(); nextLine(), next(); nextInt(); nextInt(); skip("\n").
     * <p>
     * If file does not exists, or is directory, or can not be read, method just ignores it and does nothing.
     *
     * @param file File from which comics will be loaded.
     */
    public void load(File file) {


        try (FileReader fr = new FileReader(file);
             Scanner scanner = new Scanner(fr)) {
            int numberOfComics = scanner.nextInt();

            for (int i = 0; i < numberOfComics; i++) {

                Comic comic = new Comic();
                String title = scanner.nextLine();
                String author = scanner.nextLine();
                String series = scanner.nextLine();
                String sCover = scanner.next();
                int publishMonth = scanner.nextInt();
                int publishYear = scanner.nextInt();
                scanner.skip("\n");

                comic.setTitle(title);
                comic.setAuthor(author);
                comic.setSeries(series);
                comic.setCover(Comic.Cover.valueOf(sCover));
                comic.setPublishMonth(publishMonth);
                comic.setPublishYear(publishYear);

                comics.add(comic);

            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    /**
     * Counts all comics with the same provided series name.
     *
     * @param series Name of the series for which comics will be counted.
     * @return Number of comics from the same provided series.
     */

    public int countBySeries(String series) {
        int countS = 0;
        for (Comic comic : comics) {
            if (comic.getSeries().equals(series)) {
                countS++;
            }
        }
        return countS;
    }


    /**
     * Counts all comics with the same provided author.
     *
     * @param author Author for which comics will be counted.
     * @return Number of comics with the same author.
     */
    public int countByAuthor(String author) {
        int countA = 0;
        for (Comic comic : comics) {
            if (comic.getAuthor().equals(author)) {
                countA++;
            }
        }
        return countA;
    }


    /**
     * Counts all comics with the same provided publish hear.
     *
     * @param year Publish year for which comics will be counted.
     * @return Number of comics from the same provided publish year.
     */
    public int countByYear(int year) {
        int countY = 0;
        for (Comic comic : comics) {
            if (comic.getPublishYear() == year) {
                countY++;
            }
        }
        return countY;
    }

    /**
     * Counts all comics with the same provided publish hear and month.
     *
     * @param year  Publish year for which comics will be counted.
     * @param month Publish mnt for which comics will be counted.
     * @return Number of comics from the same provided publish year and month.
     */
    public int countByYearAndMonth(int year, int month) {

        int counterYM = 0;
        for (Comic comic : comics) {
            if (comic.getPublishYear() == year && comic.getPublishMonth() == month) {
                counterYM++;
            }
        }
        return counterYM;

    }

    /**
     * Removes all comics with publish year smaller than the provided year. For the removal process
     * method uses iterator.
     *
     * @param year Provided yer.
     */
    public void removeAllOlderThan(int year) {
        //throw new UnsupportedOperationException();
        Iterator iteratorAge = comics.iterator();

        while (iteratorAge.hasNext()) {
            Comic comic = (Comic) iteratorAge.next();

            if (comic.getPublishYear() < year) {
                iteratorAge.remove();
            }
        }
        for (Comic comic : comics) {
            if (comic.getPublishYear() < year) {
                comics.remove(comic);
            }
        }
    }


    /**
     * Removes all comics written by the specified author. For the removal process method uses iterator.
     *
     * @param author Provided author.
     */
    public void removeAllFromAuthor(String author) {
        Iterator iteratorA = comics.iterator();
        while (iteratorA.hasNext()) {
            Comic comic = (Comic) iteratorA.next();
            if (comic.getAuthor().equals(author)) {
                iteratorA.remove();
            }
        }
    }

    /**
     * Creates specified map and returns it.
     *
     * @return Mapping author->comics. Map keys are names of the authors (String) present in the library. Map values are
     * collection (e.g.: HashSet<Comic>) of comics for specified author.
     */

    public Map<String, Collection<Comic>> getAuthorsComics() {


        Map<String, Collection<Comic>> mapAuthorComic = new TreeMap<>();
        for (Comic comic : comics) {
            if (mapAuthorComic.containsKey(comic.getAuthor())) {
                Set<Comic> comicFromAuthor = (Set) mapAuthorComic.get(comic.getAuthor());
                comicFromAuthor.add(comic);
            } else {
                Set<Comic> comicFromAuthor = new HashSet<>();
                comicFromAuthor.add(comic);
                String authorOfComic = comic.getAuthor();
                mapAuthorComic.put(authorOfComic, comicFromAuthor);
            }
        }
        return mapAuthorComic;
    }

    /**
     * Creates specified map and returns it.
     *
     * @return Mapping publish year->comics. Map keys are publish year (Integer, generics can not be simple types
     * instead int the Integer is used) present in the library. Map values are collection (e.g.: HashSet<Comic>)
     * of comics for specified author.
     */

    public Map<Integer, Collection<Comic>> getYearsComics() {
        Map<Integer, Collection<Comic>> mapYearComic = new TreeMap<>();


        for (Comic comic : comics) {
            if (mapYearComic.containsKey(comic.getPublishYear())) {
                Set<Comic> comicFromAuthor = (Set) mapYearComic.get(comic.getPublishYear());
                comicFromAuthor.add(comic);
            } else {
                Set<Comic> comicFromYear = new HashSet<>();
                comicFromYear.add(comic);
                Integer yearOfComic = comic.getPublishYear();
                mapYearComic.put(yearOfComic, comicFromYear);
            }
        }
        return mapYearComic;

    }

    public void removeAllNewerThan(int year) {

        Iterator iterator = comics.iterator();
        while (iterator.hasNext()) {
            Comic comic = (Comic) iterator.next();
            if (comic.getPublishYear() > year) {
                iterator.remove();
            }
        }
    }

}

